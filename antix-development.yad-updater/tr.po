# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# mahmut özcan <mahmutozcan@protonmail.com>, 2021
# Huckleberry Finn, 2022
# Mehmet Akif 9oglu, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:53+0200\n"
"PO-Revision-Date: 2020-02-27 12:11+0000\n"
"Last-Translator: Mehmet Akif 9oglu, 2023\n"
"Language-Team: Turkish (https://app.transifex.com/anticapitalista/teams/10162/tr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: yad-updater:13
msgid "antiX - Updater"
msgstr "antiX - Güncelleyici"

#: yad-updater:14
msgid "Internet connection detected"
msgstr "İnternet bağlantısı algılandı"

#: yad-updater:15
#, sh-format
msgid ""
"\\n <b><big><big>  It seems that apt is already in use.  </big></big></b> "
"\\n \\n Close any application using it (like Package Installer or Synaptic) "
"and try to run $title again. \\n \\n (Please note that you can only run one "
"instance of $title at a time) \\n"
msgstr ""
"\\n <b><big><big>Görünüşe göre apt kullanımda.</big></big></b> \\n \\n Bunu "
"kullanan herhangi bir uygulamayı (Paket Yükleyici veya Synaptic gibi) kapat "
"ın ve $title’ı tekrar çalıştırmayı deneyin. \\n \\n (Bir seferde yalnızca "
"bir $title örneğini çalıştırabileceğinizi lütfen unutmayın) \\n"

#: yad-updater:16
msgid "No Internet connection detected"
msgstr "İnternet bağlantısı algılanmadı"

#: yad-updater:17
msgid "You are Root or running the script in sudo mode"
msgstr "Köksünüz veya komut dosyasını sudo modunda çalıştırıyorsunuz"

#: yad-updater:18
msgid "You entered the wrong password or you cancelled"
msgstr "Yanlış parola girdiniz veya iptal ettiniz"

#: yad-updater:19
msgid "Waiting for a Network connection..."
msgstr "Ağ bağlantısı bekleniyor..."

#: yad-updater:20
msgid "\\n No updates were found \\n Your system is up to date \\n"
msgstr "\\n Güncelleme bulunamadı \\in Sisteminiz güncel \\n"

#: yad-updater:21
msgid "\\n Your system is now updated \\n"
msgstr "\\n Sisteminiz artık güncel\\n"

#: yad-updater:22
msgid ""
"\\n <b><big><big>  There was an error while updating your system! "
"</big></big></b> \\n \\n Please read the output in the Terminal window below"
" this one. \\n After closing this window, you can try to fix the problem by "
"running <b> sudo apt -f install  </b> \\n in a new Terminal window and then "
"try to update your system again. \\n \\n If the problem still remains, copy "
"any error messages that appear in the Terminal and \\n ask for help at "
"antixforum.com \\n"
msgstr ""
"\\n <b><big><big>Sisteminiz güncellenirken bir hata oluştu! "
"</big></big></b>\\n \\n Lütfen bunun altındaki Terminal penceresindeki "
"çıktıyı okuyun. \\n Bu pencereyi kapattıktan sonra yeni bir Terminal "
"penceresinde <b>sudo apt -f install </b> \\n komutunu çalıştırarak sorunu "
"çözmeyi deneyebilir ve ardından sisteminizi tekrar güncellemeyi "
"deneyebilirsiniz. \\n \\n Sorun devam ederse, Terminal’de görünen hata "
"mesajlarını kopyalayın ve \\n antixforum.com’dan yardım isteyin \\n"

#: yad-updater:23
msgid "antiX - Updater: DO NOT CLOSE THIS WINDOW"
msgstr "antiX - Güncelleyici: BU PENCEREYİ KAPAT MAYIN"

#: yad-updater:24
msgid "'Automatic' Update"
msgstr "‘Otomatik’ güncelle"

#: yad-updater:25
msgid "'Manual' Update"
msgstr "‘Elle’ güncelle"

#: yad-updater:26
msgid "<b><big><big> There's, at least, "
msgstr "<b><big><big>En azından,"

#: yad-updater:27
msgid ""
" available update(s) </big></big></b> \\n \\n <b> 'Automatic' Update </b> - "
"usually it's completely unattended. and normally safe, since it always makes"
" the default choices for you  \\n (Pressing the Enter key now will select "
"the Automatic mode) \\n <b> 'Manual' Update </b> - gives you full control of"
" the update process, but may require you to make some choices \\n"
msgstr ""
"güncelleme(ler) mevcut </big></big></b>\\n \\n <b>‘Otomatik’ Güncelleme </b>"
" - genellikle tamamen gözetimsizdir. ve normalde güvenlidir, çünkü her zaman"
" sizin için varsayılan seçimleri yapar \\n (Enter tuşuna basıldığında "
"Otomatik mod seçilecektir) \\n <b> ‘Elle’ Güncelleme </b> - size güncelleme "
"işleminin tam kontrolünü verir, ancak bazı işlemler yapmanızı gerektirebilir"
" \\n"
