��          T      �       �      �       �   #   �   /     /   K     {  h  �  3   �  8   %  >   ^  a   �  O   �     O                                        Internet connection detected No Internet connection detected! Waiting for a Network connection... You are Root or running the script in sudo mode You entered the wrong password or you cancelled already root Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-12-27 14:16+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2022
Language-Team: Ukrainian (https://app.transifex.com/anticapitalista/teams/10162/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Виявлено Інтернет-з'єднання Інтернет-з'єднання не виявлено Очікування мережевого з'єднання... Ви є користувачем Root або виконуєте скрипт у режимі sudo Ви ввели неправильний пароль або скасували вже є root 