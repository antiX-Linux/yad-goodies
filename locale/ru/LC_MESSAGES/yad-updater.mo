��          �      �       H     I     \  !   l     �     �  #   �  /   �  /     �   O  8   M      �     �  )   �  �  �  5   �  %     <   7  A   t  F   �  ;   �  e   9  E   �  �  �  \   �	  =   0
     n
  G   �
                  	                 
                              'Automatic' Update 'Manual' Update <b><big><big> There's, at least,  Internet connection detected No Internet connection detected Waiting for a Network connection... You are Root or running the script in sudo mode You entered the wrong password or you cancelled \n <b><big><big>  It seems that apt is already in use.  </big></big></b> \n \n Close any application using it (like Package Installer or Synaptic) and try to run $title again. \n \n (Please note that you can only run one instance of $title at a time) \n \n No updates were found \n Your system is up to date \n \n Your system is now updated \n antiX - Updater antiX - Updater: DO NOT CLOSE THIS WINDOW Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-27 12:11+0000
Last-Translator: Andrei Stepanov, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 «Автоматическое» обновление «Ручное» обновление <b><big><big> Доступно по крайней мере  Обнаружено подключение к Интернету Подключение к Интернету не обнаружено Ожидание сетевого подключения... Вы - суперпользователь или запустили скрипт в sudo режиме Вы ввели неверный пароль или отменили \n <b><big><big> Похоже, что APT уже используется. </big></big></b> \n \n  Закройте любые приложения, использующие его (например, Установщик пакетов или Synaptic), и попробуйте запустить $title снова. \n \n (Пожалуйста, обратите внимание, что одновременно можно запустить только один экземпляр $title) \n \n Обновления не найдены \n Ваша система обновлена \n \n Ваша система теперь обновлена \n antiX - Обновление antiX - Обновление: НЕ ЗАКРЫВАЙТЕ ЭТО ОКНО 