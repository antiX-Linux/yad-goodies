��          \      �       �      �       �   #     /   +  /   [     �     �  w  �  #   #  0   G  '   x  4   �  +   �                                                 Internet connection detected No Internet connection detected! Waiting for a Network connection... You are Root or running the script in sudo mode You entered the wrong password or you cancelled already root antiX - autoremove Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-12-27 14:16+0000
Last-Translator: Paulo C., 2024
Language-Team: Galician (Spain) (https://app.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Detectada unha conexión a Internet Non fo idetectada ningunha conexión a Internet! Agardando por unha conexión á rede... Es administrador ou executando o script en modo sudo Contrasinal incorrecta ou acción cancelada xa como administrador Eliminar automático de antiX 