# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# marcelo cripe <marcelocripe@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:51+0200\n"
"PO-Revision-Date: 2021-12-27 14:16+0000\n"
"Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2021\n"
"Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: yad-autoremove:12
msgid "Internet connection detected"
msgstr "Foi detectada a conexão com a Internet"

#: yad-autoremove:17
msgid "already root"
msgstr "você já está como usuário root"

#: yad-autoremove:21
msgid "You are Root or running the script in sudo mode"
msgstr ""
"Você já está utilizando o Root ou executando a sequência de instruções "
"(script) no modo sudo"

#: yad-autoremove:23
msgid "You entered the wrong password or you cancelled"
msgstr "Você inseriu a senha errada ou você cancelou"

#: yad-autoremove:30
msgid "Waiting for a Network connection..."
msgstr "Aguardando por uma conexão de rede ..."

#: yad-autoremove:30 yad-autoremove:34
msgid "antiX - autoremove"
msgstr "Limpeza Automática do antiX"

#: yad-autoremove:39
msgid "No Internet connection detected!"
msgstr "Nenhuma conexão com a Internet foi detectada!"
