# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# heskjestad <cato@heskjestad.xyz>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:51+0200\n"
"PO-Revision-Date: 2021-12-27 14:16+0000\n"
"Last-Translator: heskjestad <cato@heskjestad.xyz>, 2021\n"
"Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nb\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: yad-autoremove:12
msgid "Internet connection detected"
msgstr "Fant internett-tilkobling"

#: yad-autoremove:17
msgid "already root"
msgstr "allerede root"

#: yad-autoremove:21
msgid "You are Root or running the script in sudo mode"
msgstr "Du er root eller kjører skriptet med sudo"

#: yad-autoremove:23
msgid "You entered the wrong password or you cancelled"
msgstr "Du skrev inn feil passord, eller avbrøt"

#: yad-autoremove:30
msgid "Waiting for a Network connection..."
msgstr "Venter på nettverkstilkobling …"

#: yad-autoremove:30 yad-autoremove:34
msgid "antiX - autoremove"
msgstr "antiX – autofjerning"

#: yad-autoremove:39
msgid "No Internet connection detected!"
msgstr "Fant ingen internett-tilkobling!"
