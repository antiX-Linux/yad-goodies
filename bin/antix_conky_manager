#! /bin/bash
#Simply conky manager for antiX Linux, by PPC, GPL license, 2024
#Change log:
#version 1.4- Revamped and simplified the funtions that set Main and Accent colors in Modern Conky, also reordered buttons on main screen
#version 1.3- Added "Conky position" button, fixed capilal letters in wlan and eth indicators
#version 1.2- Compacted the code. Simplified the time&date display, making it also look better. Added button to toggle Disk Input/Output (off by default). Added a button to toggle the Modern Accent colour. Added button to toggle the main font; Added, to the Modern Conky configuration file, code that allows to display the top 5 processes (but added no GUI to toggle that, to stop "Feature creap"- if more advanced users want that feature, they just have to uncomment the line); Added button to toggle the Original Conky theme; Edited the modern conky template to always apply the correct color and font to the displayed text.
#version 1.0b- fixes the file loading funtion so it works with paths that include spaces
#version 0.9- includes on/off toggle for a Monthly calendar. Added icons and tooltips to the "Modern Conky 'Applet' Manager". Selected a new, more international, and Linux related, default RSS; fixed some small typos and mistakes
#version 0.8- includes on/off toggles for Date & Time; Sytem Info; Network info. On the main window, added button to load a conky configuration file. Disabled battery indicatior on by default
#version 0.5- improved /tmp/.conkyrc adding a title, so the GUI can detect if the "Modern Conky" is being used and also fixed the Weather; Added Toggle to turn Weather on/off  
#version 0.4- improved /tmp/.conkyrc, made it easier to understand and edit
#version 0.3- changed button's text, 16/01/2024
#added the possibility to use the "modern conky", that displays the weather- the script offers to install the required font, if needed; added creation and restorations of a back-up. Added icons to buttons

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=antix_conky_manager

#Strings to be localized:
toggle_button=$"Permanently toggle Conky on/off"
restore_default_conky_button=$"Switch to the default Original Conky Configuration"
force_conky_refresh_button=$"Refresh information displayed in Conky"
manually_edit_conky_button=$"Manually edit current Conky configuration file"
window_title=$"Conky Manager"
main_window_text=$"Conky is antiX's desktop resource monitor application. \n It can be configured to display time and date, system resources, the weather, news, contents of text files, etc"
change_conky_profile_button=$"Switch to (or reset) the 'Modern' Conky configuration"
mv_OK=$"OK"
mv_x=$"Cancel"
restore_conky_from_backup_button=$"Switch to the last backed up Conky configuration"
create_backup_button=$"Back up current Conky configuration"
manage_modern_conky_button=$"Manage 'Modern' Conky 'applets' and settings"
load_a_conky_button=$"Select a Conky configuration file to load"
select_default_conky_theme_button=$"Select color theme for the Original Conky configuration"
conky_position_button=$"Conky position"

#If a backup copy of the conky configuration file does not exist, create one.
[ -e ~/.conkyrc_backup ] || cp ~/.conkyrc ~/.conkyrc_backup

#######CREATE 'modern conky0 configuration file in the temporary folder:
cat > "/tmp/.conkyrc" << 'FILE_TEMPLATE_A'
# Modern Conky configuration file
# For conky editing help and commands visit = Casey's Conky Reference with Examples {http://www.ifxgroup.net/conky.htm}

###Color profiles that you can use in conky sections, using this example "color_name ffffff" (note "color2" is used as color for the title, when displayed:
color2 orange
#color4 yellow
#color8 77ccff
#color9 5599cc

### Default colors and also border colors (default is white, colors can be, for example: black, yellow, green, red, pink, etc):
default_color white
default_shade_color black
default_outline_color black

### Use Xft (default is yes):
use_xft yes

### X font when Xft is disabled, you can pick one with program xfontsel (uncomment the value you want to use, make sure the other values are commented):
font 8x12
#font 7x12
#font 6x10
#font 7x13
#font 7x12

### Generic font when Xft is enabled  (uncomment the value you want to use, make sure the other values are commented):
xftfont Carlito:size=10
#ftfont DejaVu Sans:bold:size=9
#xftfont gentium:size=12
#xftfont DejaVu Sans:size=10
#xftfont DejaVu Sans:size=9

####Does conky run on the background? (set to yes if you want Conky to be forked in the background)
background yes

### Generic configurations:
short_units yes
cpu_avg_samples 1
net_avg_samples 1
out_to_console no

### Create own window instead of using desktop (required in nautilus, pcmanfm and rox desktops, probably also in zzzfm):
own_window yes
own_window_transparent yes
own_window_hints undecorated,below,sticky,skip_taskbar
own_window_type normal

### Text alpha when using Xft:
xftalpha 1.0

#on_bottom no

### Update interval in seconds:
update_interval 1

### Use double buffering (reduces flicker, may not work for everyone) (default is yes):
double_buffer yes

### Minimum size of text area:
minimum_size 55
maximum_width 550

### Draw shades around characters (default is no):
draw_shades no

### Draw outlines around characters (default is no):
draw_outline yes

### Draw borders around the text area (default is no):
draw_borders no

### Stippled borders (default is 0):
stippled_borders 0

### border margins (default is commented line, i.e., nothing):
#border_margin 10

### Border width (default is 2):
border_width 2

### Text alignment:  (uncomment the value you want to use, make sure the other values are commented)
alignment top_right
#alignment top_middle
# alignment top_left
#alignment bottom_left
#alignment bottom_right

### Gap between borders of screen and text:
gap_x 20
gap_y 20

### Add spaces to keep things from moving about  (This only affects certain objects):
use_spacer right

### Subtract file system buffers from used memory:
no_buffers yes

### if_up_strictness link. Allowed values:  up | link | address (default is "if_up_strictness address"):
if_up_strictness address

###Display all text in UPPERCASE (set to yes if you want all text to be in uppercase, the default is "uppercase no"):
uppercase no

###SECTION DIRECTLY RELATED TO THE KIND OF INFORMATION THAT YOU WANT TO  DISPLAYED ONSCREEN (First line always has to be "TEXT", lines with just a space are used to separate, on screen different "applets"):
TEXT
###***Show an optional line to add an horizontal separation bar, anywhere you want, by inserting the following (uncomented line):
#${color2}${voffset 2}${hr 1}
###***Show big date & time:
$color${font Carlito:size=51}${time %H:%M}${font}${voffset -64}${offset 20}${font Carlito:size=45}${font Tall Films:size=20}${time %d} ${font Tall Films:size=20}${time %B} ${time %Y}${font}${voffset 19}${font Tall Films:size=40}${offset -210}${time %A}${font}

###***Show the calendar for the current month:
#${font Carlito:size=10}${execpi 600 ncal}
###You may comment out the lines referring to the resources you do not want to be displayed, or keep the line and delete what you don't want
###***Show Weather (an empty line after that one is only to have an empty space before the rest of conky's output:
${font Symbola:size=30}${execpi  100 curl wttr.in/?format=4| cut -d ':' -f2|sed 's/+//g'|sed 's/🌡️/  /g'|sed 's/🌬️/  /g'}
###***Show Indicators for HD RAM CPU SWAP:
${voffset 1}${offset 12}${font}${color2} HD ${offset 9}$color${fs_free /} / ${fs_size /}${offset 15}${color2}RAM ${offset 9}$color$mem / $memmax${offset 10}${color2}CPU ${offset 2}$color${cpu cpu0}%  ${color2} SWAP $color $swap / $swapmax
###***Show Disk Input/Output (commented by default):
#${offset 12}${color2}${font} DISK I/O  ${color}${diskio}
###***Show Indicators for Ethernet (eth0) or Wireless (wlan0) connections, etc:
${if_existing /proc/net/route eth0} ${offset 12}${font Carlito:pixelsize=12}${color2}ETH0↑  ${font}$color${upspeed eth0}  ${offset 12}${font Carlito:pixelsize=12}${color2}${offset 55}ETH0↓  ${font}$color${downspeed eth0} ${endif}${if_existing /proc/net/route wlan0} ${offset 12}${font Carlito:pixelsize=12}${color2}WLAN0↑  ${font}$color${upspeed wlan0}  ${voffset 1}${font Carlito:pixelsize=12}${color2}${offset 55}WLAN0↓  ${font}$color${downspeed wlan0} ${endif} ${if_existing /proc/net/route eth1} ${offset 12}${font Carlito:pixelsize=12}${color2}ETH1↑  ${font}$color${upspeed eth1}  ${offset 12}${font Carlito:pixelsize=12}${color2}${offset 55}ETH1↓  ${font}$color${downspeed eth1} ${endif} ${if_existing /proc/net/route wlan1} ${offset 12}${font Carlito:pixelsize=12}${color2}WLAN1↑  ${font}$color${upspeed wlan1}  ${offset 12}${font Carlito:pixelsize=12}${color2}${offset 55}WLAN1↓  ${font}$color${downspeed wlan1} ${endif}
###***Show Battery indicator (commented by default):
#${if_existing  /proc/acpi/battery BAT0}  ${voffset 1}${offset 12}${font}${color2} Bat.  $color${battery_percent BAT0}% ${endif} ${if_existing  /proc/acpi/battery BAT1}  ${voffset 1}${offset 12}${font}${color2} Bat.  $color${battery_percent BAT1}% ${endif}
###***Show name of current Desktop (commented by default):
#${voffset 1}${offset 15}${font}${color2}DESKTOP ${color}${exec disp=${DISPLAY#:}; disp=${disp%.[0-9]}; cat $HOME/.desktop-session/desktop-code.$disp 2>/dev/null}
#${color}${voffset 2}${hr 1}
###*** Show Caps Lock and Num lock indicators (commented by default)
#${offset 12}${color2}${font} Caps Lock:${exec xset q | grep "Caps Lock" | awk '{print $4}'}${color}  ${color2}	$font${exec xset q | grep "Num Lock" | awk '{print $6 $7 $8'}${color}}
###***Top running processes (one line is for title of the section, then 1 for each of the top processes, all commented by default):
#${offset 15}${font}${color2}TOP PROCESSES 
#${offset 15}${font}${color}${top name 1}${alignc}CPU:${top cpu 1}%
#${offset 15}${font}$color${top name 2}${alignc}CPU:${top cpu 2}%
#${offset 15}${font}$color${top name 3}${alignc}CPU:${top cpu 3}%
#${offset 15}${font}$color${top name 4}${alignc}CPU:${top cpu 4}%
#${offset 15}${font}$color${top name 5}${alignc}CPU:${top cpu 5}%

###***Show RSS news feed (lines commented by default): 
###Optional title of the news feed, that will be displayed on screen (uncomment the following line and edit it do display the text you want)
#${color2} RSS FEED
###RSS feed you want conky to display (the last number is how many lines of the feed we want conky to display; uncomment the following line and edit it do display the text you want)
#${font}$color${rss http://rss.slashdot.org/Slashdot/slashdotLinux 300 item_titles 15 }
###*** Show the contents of a text file (by default it's ~/.icewm/keys) (commented by default):
#${font}$color${execi 600 cat ~/.icewm/keys}

FILE_TEMPLATE_A

#Generic functions:
permantently_toggle_conky()
{
 #script to enable/disable conky, also enabling/disabling it from ~/.desktop-session/startup, by making sure  it's entry is uncommentend/commented) 
 file=$HOME/.desktop-session/desktop-session.conf

 if [ "$(pidof conky)" ]; then

 #	echo "conky is running, turning it off"
	pkill conky  &

 # make sure conky is disabled on the desktop-session file
	sed -i 's/LOAD_CONKY="true"/LOAD_CONKY="false"/' "$file"
	yad --borders=10 --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --text=$"Conky will now be disabled" --button=" x "

        
  else	
	#make sure conky is enabled on the desktop-session file
	sed -i 's/LOAD_CONKY="false"/LOAD_CONKY="true"/' "$file"
	yad --borders=10 --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --text=$"Conky will now be enabled" --button=" x "

	# echo "conky is not running, starting it"
	conky &
 	
 fi	
} # end of permantently_toggle_conky

restore_default_conky()
{
 #script to restore conky from the default configuration, stored in the skel folder
 yad --center --borders=10 --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title=$"Warning!" --text=$"This will restore Conky to it's original form. \n You will lose any costumization you performed. \n Are you sure you want to proceed?"
 # Check if the OK button was pressed
 if [ "$?" -eq 0 ]; then
	#User pressed "Ok", so copy the file to the home folder
    cp /etc/skel/.conkyrc $HOME
   else
    #User pressed "Cancel", so exit
    exit
 fi

} # end of restore_default_conky

force_conky_refresh()
{
 #Conky to force reload conky	
 pkill conky &
 sleep 1
 conky &
} # end of force_conky_refresh

manually_edit_conky()
{
 #Manually edit conky's config file in geany Text Editor	
 geany $HOME/.conkyrc
	
} # end of manually_edit_conky

select_conky_profiles()
{
#For now, just allows to select the "modern" conky configuration	
#Check if the symbola font is installed, if so, just use the "modern" template, if not suggest to install the font first.
 package_name="fonts-symbola"
 if dpkg -s "$package_name" >/dev/null 2>&1; then
	# package is installed, continue with script
     test=ok
     else
		# package is not installed, try to install it before continuing with script
		yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20   --text="$package_name is not installed.\n It is required for some of the 'modern' Conky functionality. \n Do you want to install it now?" --button=$"OK":1 --button=$" x ":2 
		foo=$?
		#This next line zeroes the file that is used to flag the end of the install process (in the end of the follwoing if statement, the "; echo 1 > /tmp/finished1" part changes that's file's content, signaling that the process is over and the main script can continue running:
		echo 0 > /tmp/finished1
			if [[ $foo -eq 1 ]]; then x-terminal-emulator -e /bin/bash -c "gksu 'apt update' && gksu 'apt install -y fonts-symbola'; echo 1 > /tmp/finished1"
			fi
		#wait until install process is finished (this waits until the "sudo apt install" process running in paralel echoes the value 1 to the /tmp/finished1 file:
		finished=$(cat /tmp/finished1)
		until [ $finished -gt 0 ]
		do
		finished=$(cat /tmp/finished1)
		done
 fi		

 #Use the modern conkyrc file
 cp /tmp/.conkyrc $HOME
 #force restart conky
 pkill conky &
 sleep 0.5
 conky &
} # end of select_conky_profiles

restore_conky_from_backup()
{
 #If a back-up file of conky's configuration exists, restore it and restart conky	
 conkyrc_backup="$HOME/.conkyrc_backup"
 conkyrc="$HOME/.conkyrc"

 if [ -e "$conkyrc_backup" ]; then
	yad --center --borders=10 --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title=$"Warning!" --text=$"This will restore Conky to the last back-up of it's configuration. \n You will lose any costumization you performed. \n Are you sure you want to proceed?"
	# Check if the OK button was pressed
	if [ "$?" -eq 0 ]; then
		#User pressed "Ok", restore the configuration
		cp $HOME/.conkyrc_backup $HOME/.conkyrc
		pkill conky &
		sleep 1
		conky &
		yad --fixed --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --text=$"The backup copy of Conky's configuration file was restored" --button=$"OK"
	  else
		#User pressed "Cancel", so exit
		exit
	fi	

    else
		yad --fixed --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --text=$"The Backup file does not exist: $conkyrc_backup \n If you wish, you can still use the option to restore the original Conky configuration" --button=$"OK"

 fi
} # end of restore_conky_from_backup

create_backup_copy()
{
 backup_file="$HOME/.conkyrc_backup"	
 #Create a backup copy of the corrent conky configuration file
 #If a backup file already exists, display a warning
 if [ -e "$backup_file" ]; then yad --center --borders=10 --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title=$"Warning!" --text=$"Doing this will automatically replace the pre-existing back-up creating using this GUI! \n Are you sure you want to proceed?"
	# Check if the OK button was pressed
	if [ "$?" -eq 0 ]; then
    #User pressed "Ok", so copy the file to the home folder
    choice=ok
	else
		#User pressed "Cancel", so exit
		exit
	fi
 fi		
 #backup current conky configuration and display a warning, when done.
 cp ~/.conkyrc ~/.conkyrc_backup && yad --center --borders=10 --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title=$"Conky" --text=$"The current configuration file was backed up to the hidden file ~/.conkyrc_backup" --button=$"OK"  
} # end of create_backup_copy

load_a_conky()
{
 #Window for user selection of file
 selected_file=$(yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --file --text=$"Select a file to be used as .conkyrc (Conky configuration file). To see hidden files you may have to press CTRL + H" --width=790)

 #Check if user made a selection	(i.e. if the selected file exists)
 if [ -e "$selected_file" ]; then
		#User, in fact, selected a file, to copy it to be used as .conkyrc and restart conky
    	cp "$selected_file" $HOME/.conkyrc
		pkill conky &
		sleep 1
		conky &
     else
		#User did not make a selection, canceled or closed the file selection window, so just display a warning
		yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --text=$"You made no selection. \n No change will be made." --button=$"OK"
 fi
 exit
} # end of load_a_conky

select_default_conky_theme()
{
 if ! grep -q "Modern Conky" "$HOME/.conkyrc"; then
    yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --fixed --columns=1 --form --text-align=center --text=$"Select a theme for the Original Conky configuration:" --no-buttons\
    --field=$"White":FBTN "bash -c 'conky-colors white'" \
    --field=$"Light-grey":FBTN "bash -c 'conky-colors light-grey'" \
    --field=$"Light":FBTN "bash -c 'conky-colors light'"\
    --field=$"Light-blue":FBTN "bash -c 'conky-colors light-blue'" \
    --field=$"Light-green":FBTN "bash -c 'conky-colors light-green'" \
    --field=$"Light-red":FBTN "bash -c 'conky-colors light-red'" \
    --field=$"Dark":FBTN "bash -c 'conky-colors dark'" \
    --field=$"Darker-blue":FBTN "bash -c 'conky-colors darker-blue'" \
    --field=$"Dark-blue":FBTN "bash -c 'conky-colors dark-blue'" \
    --field=$"Dark-purple":FBTN "bash -c 'conky-colors dark-purple'" \
    --field=$"Dark-red":FBTN "bash -c 'conky-colors dark-red'" \
    --field=$"RANDOM":FBTN "bash -c 'conky-colors random'" \
    --columns=2
   
   else

    yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --text=$"You don't seem to be using the Original Conky configuration." --button=$"OK"
    
 fi   
} # end of select_default_conky_theme
 
conky_aligment()
{
 sed -i "s/^alignment .*/alignment $1/"  $HOME/.conkyrc

 #force restart conky
 pkill conky &
 sleep 0.5
 conky &
} # end of  conky_aligment

manage_conky_aligment()
{
 export -f  conky_aligment
 yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title=$"Conky Position" --borders=20 --fixed --columns=3 --form --text-align=center --text=$"You can manage the following Modern Conky options:" --no-buttons\
    --field=$"Top left":FBTN "bash -c 'conky_aligment top_left'" \
    --field=$"Middle left":FBTN "bash -c 'conky_aligment middle_left'" \
    --field=$"Bottom left":FBTN "bash -c 'conky_aligment bottom_left'" \
    --field=$"Top Middle":FBTN "bash -c 'conky_aligment top_middle'" \
    --field=$"Middle middle":FBTN "bash -c 'conky_aligment middle_middle'" \
    --field=$"Bottom middle":FBTN "bash -c 'conky_aligment bottom_middle'"\
    --field=$"Top Right":FBTN "bash -c 'conky_aligment top_right'"\
    --field=$"Middle right":FBTN "bash -c 'conky_aligment middle_right'" \
    --field=$"Bottom right":FBTN "bash -c 'conky_aligment bottom_right'" 
 
 exit
 
} # end of manage_conky_aligment

#Modern Conky management functions:
toggle_modern_conky_option()
{
 #Toogle commenting on/off line of the file $HOME/.conkyrc that includes the text inputed to the function from the yad selection window
 local search_text="$1"

 # Check if the file exists
 if [ -e "$HOME/.conkyrc" ]; then
    # Use grep to find the line number containing the search text
    line_number=$(grep -n "$search_text" $HOME/.conkyrc | cut -d: -f1)

    # Check if the search text was found
    if [ -n "$line_number" ]; then
        echo "The line number of '$search_text' in $HOME/.conkyrc is: $line_number"
      else
        echo "The text '$search_text' was not found in $HOME/.conkyrc"
    fi
   else
    echo "File '$file_name' does not exist."
 fi	

 sed -n "${line_number}p" $HOME/.conkyrc | grep -qE '^[[:space:]]*#'
 if [ $? -eq 0 ]; then
	echo " $line_number is commented, uncommenting it"
	sed -i "${line_number}s/^#//" $HOME/.conkyrc
   else
	echo " $line_number is not commented, commenting it"
	sed -i "${line_number}s/^/#/" $HOME/.conkyrc
 fi

 #force restart conky
 pkill conky &
 sleep 0.5
 conky &
} # end of toggle_modern_conky_option

toggle_systeminfo_in_modern_conky()
{
 #Toggle SystemInfo	
 file_name="$HOME/.conkyrc"
 search_text='$color$mem'

 # Check if the file exists
 if [ -e "$file_name" ]; then
    # Use grep to find the line number containing the search text
    line_number=$(grep -n "$search_text" "$file_name" | cut -d: -f1)

    # Check if the search text was found
    if [ -n "$line_number" ]; then
        echo "The line number of '$search_text' in '$file_name' is: $line_number"
    else
        echo "The text '$search_text' was not found in '$file_name'"
    fi
   else
    echo "File '$file_name' does not exist."
 fi	

 sed -n "${line_number}p" "$file_name" | grep -qE '^[[:space:]]*#'
 if [ $? -eq 0 ]; then
    echo " $line_number is commented, uncommenting it"
    sed -i "${line_number}s/^#//" "$file_name"
   else
    echo " $line_number is not commented, commenting it"
    sed -i "${line_number}s/^/#/" "$file_name"
 fi

 #force restart conky
 pkill conky &
 sleep 0.5
 conky &
} # end of 

toggle_default_color()
{
	
 #Mini script to edit conky's default color, by PPC, 17/1/2024
 color=$(yad --width=400 --title=$"Conky main color (text, border, etc)" --center --color --gtk-palette)

 if [ -n "$color" ]; then
    # not empty
    color="${color:1}"
    sed -i "s/^default_color .*/default_color $color/" $HOME/.conkyrc
	pkill conky &
	sleep 0.5
	conky &
 fi
 
 exit

} # end of toggle_default_color

toggle_accent_color()
{
 #Mini script to edit conky's color2, by PPC, 17/1/2024
 color=$(yad --width=400 --title=$"Conky main color (text, border, etc)" --center --color --gtk-palette)

 if [ -n "$color" ]; then
    # not empty
    color="${color:1}"
    sed -i "s/^color2 .*/color2 $color/"  $HOME/.conkyrc
	pkill conky &
	sleep 0.5
	conky &
 fi
 
 exit

} # end of toggle_accent_color

select_default_font()
{
 #default conky font:
 current_font=$(cat $HOME/.conkyrc|grep ^xftfont | sed 's/xftfont//g'|sed 's/size=//g')
 selected_font=$(yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --fixed --font --text=$"The current font is $current_font \n(The default font is Carlito:10)" --button="OK")
 first_word=$(echo $selected_font | awk '{print $1}')
 last_word=$(echo $selected_font | awk '{print $NF}')
 modern_conky_default_font=$(echo ${first_word}:size=${last_word})
 if [ -n "$selected_font" ]; then
	echo $modern_conky_default_font
	sed -i "s/^xftfont .*/xftfont $modern_conky_default_font/"  $HOME/.conkyrc
	pkill conky &
	sleep 0.5
	conky &
 fi
 exit
} # end of select_default_font

manage_modern_conky()
{
#Check if 'Modern conky is being used
 if grep -q "Modern Conky" "$HOME/.conkyrc"; then

    yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --fixed --columns=1 --form --text-align=center --text=$"You can manage the following Modern Conky options:" --no-buttons\
    --field=$"Time applet on/off!/usr/share/icons/papirus-antix/24x24/emblems/emblem-urgent.png! Display Time and Date, using the current locale's settings":FBTN "bash -c 'toggle_modern_conky_option  {time'" \
    --field=$"Calendar applet on/off!/usr/share/icons/papirus-antix/24x24/apps/calendar.png! Display the calendar for the current month":FBTN "bash -c 'toggle_modern_conky_option ncal'" \
    --field=$"Weather applet on/off!/usr/share/icons/papirus-antix/24x24/apps/org.gnome.Weather.png! Display the current Weather forcast for your local. If there is no internet acces or the wttr.in service is down, just a blank space may be displayed in Conky":FBTN "bash -c 'toggle_modern_conky_option  wttr.in'" \
    --field=$"System Information applet on/off!/usr/share/icons/papirus-antix/24x24/devices/cpu.png! Display Disk space, RAM usage, CPU usage and Swap usage":FBTN "bash -c toggle_systeminfo_in_modern_conky" \
    --field=$"Disk Input\Output applet on/off!/usr/share/icons/papirus-antix/24x24/devices/drive-harddisk.png! Display the amount of data being read/writen to Disk":FBTN "bash -c 'toggle_modern_conky_option {diskio}'" \
    --field=$"Network Information applet on/off!/usr/share/icons/papirus-antix/24x24/actions/kt-upnp.png! Display your Network usage, showing your current Upload and Download transfer rates":FBTN "bash -c 'toggle_modern_conky_option  /proc/net/route'" \
    --field=$"Battery Information applet on/off!/usr/share/icons/papirus-antix/24x24/panel/indicator-sensors-battery.png! If the computer has a battery, will display it's percentage":FBTN "bash -c 'toggle_modern_conky_option  BAT0'" \
    --field=$"News applet on/off!/usr/share/icons/papirus-antix/24x24/apps/com.github.allen-b1.news.png! Display a RSS feed (requires a compatible Conky, should work with antiX up to the version 22). The default rss news feed is slashdot's. To change that, manually edit the configuration file and replace the URL in one of the last lines with the one you want":FBTN "bash -c 'toggle_modern_conky_option {rss'" \
    --field=$"Select default main color!/usr/share/icons/papirus-antix/24x24/actions/color-management.png!Select the color that Conky uses for generic text, borders, etc. Allows to select from a few default colors or use a color picker)":FBTN "bash -c 'toggle_default_color'" \
    --field=$"Select default main font!/usr/share/icons/papirus-antix/24x24/actions/font-select.png!Select the font that Conky uses for generic text (does not apply to Time and date, weather, etc. Allows to select from a few default colors or use a color picker)":FBTN "bash -c 'select_default_font'" \
    --field=$"Select accent color!/usr/share/icons/papirus-antix/24x24/actions/color-management.png!Select the color that Conky uses for generic text, borders, etc. Allows to select from a few default colors or use a color picker)":FBTN "bash -c 'toggle_accent_color'"
       
   else

    yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --text=$"You don't seem to be using the Modern Conky"  --button=$"OK"
    exit
 fi	
 pkill conky &
 sleep 0.5
 conky &
} # end of manage_modern_conky

#export all functions, so they can be used in the script:
export -f  permantently_toggle_conky restore_default_conky force_conky_refresh manually_edit_conky select_conky_profiles restore_conky_from_backup create_backup_copy manage_modern_conky load_a_conky toggle_modern_conky_option toggle_systeminfo_in_modern_conky toggle_default_color toggle_accent_color select_default_font select_default_conky_theme conky_aligment manage_conky_aligment

#Check if conky is installed
if command -v conky &> /dev/null; then
    echo $"Conky is installed..."
  else
    yad --center --text=$"Conky is not installed.\n Please install it." --button="OK"
    exit
fi

#Check if ~/.conkyrc exists
if [ -e "$HOME/.conkyrc" ]; then
    echo $"$HOME/.conkyrc exists... starting antiX Conky Manager"
  else
    echo $"$HOME/.conkyrc DOES NOT exist. Creating it"
    yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="Conky" --borders=20 --fixed  --text=$"$HOME/.conkyrc does not exist, creating it using the Modern template... \n Please restart this program, after the process if finished and Toggle Conky on" --button=$"OK"
    select_conky_profiles
    pkill conky &
    sleep 2
    conky &
fi

#Main window: 
yad --center --window-icon=/usr/share/icons/papirus-antix/48x48/categories/conky-manager.png --title="$window_title" --borders=20 --fixed --columns=1 --form --text-align=center --text="$main_window_text \n" \
		--field="${toggle_button}!/usr/share/icons/papirus-antix/24x24/apps/conky-on-off.png!":FBTN "bash -c permantently_toggle_conky" \
		--field="${conky_position_button}!/usr/share/icons/papirus-antix/24x24/actions/labplot-transform-move.png!":FBTN "bash -c  manage_conky_aligment" \
		--field="${select_default_conky_theme_button}!/usr/share/icons/papirus-antix/24x24/actions/color-management.png!":FBTN "bash -c  select_default_conky_theme" \
		--field="${force_conky_refresh_button}!/usr/share/icons/papirus-antix/24x24/emblems/emblem-insync-syncing-shared.png!":FBTN "bash -c  force_conky_refresh" \
		--field="${manually_edit_conky_button}!/usr/share/icons/papirus-antix/24x24/emblems/emblem-documents.png!":FBTN "bash -c  manually_edit_conky" \
		--field="${create_backup_button}!/usr/share/icons/papirus-antix/24x24/emblems/emblem-downloads.png!":FBTN "bash -c  create_backup_copy" \
		--field="${change_conky_profile_button}!/usr/share/icons/papirus-antix/24x24/actions/calendar-go-today.png!":FBTN "bash -c  select_conky_profiles"\
		--field="${manage_modern_conky_button}!/usr/share/icons/papirus-antix/24x24/actions/calendar-go-today.png!":FBTN "bash -c  manage_modern_conky"\
		--field="${load_a_conky_button}!/usr/share/icons/papirus-antix/24x24/actions/document-open.png!":FBTN "bash -c  load_a_conky"\
		--field="${restore_conky_from_backup_button}!/usr/share/icons/papirus-antix/24x24/emblems/emblem-link.png!":FBTN "bash -c  restore_conky_from_backup" \
		--field="${restore_default_conky_button}!/usr/share/icons/papirus-antix/24x24/emblems/vcs-update-required.png!":FBTN "bash -c  restore_default_conky" \
		--no-buttons
